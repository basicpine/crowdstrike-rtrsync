# CrowdStrike RTR sync

A GitLab CI/CD to automatically sync Real Time Response scripts from a GitLab repo to a organizations CrowdStrike instance.

One issue a security team might face is having their common security scripts living on various machines/file shares. Utilizing GitLab to store security scripts is a great solution to this, but to run these scripts through CrowdStrikes Real Time Response these scripts must also be uploaded to CrowdStrike.

This means anytime a security professional updates or creates a new script they need to push it to the repo and remember to login to CrowdStrike and uploaded it. This can lead to different versions of the script existing and in a security incident could lead to frustration. This CI/CD pipeline is meant to solve this by utilizing CrowdStikes APIs and auto uploading the scripts pushed to the repo.

## Set-Up
To setup CI/CD in your environment. Refer to GitLabs documentation.  
Clone the repo  
Go to settings > CI/CD > Variables and add two masked variables called:  
 - ```client_id``` being your API id  
 - ```client_secret``` being your API secret  
 
Refer to CrowdStrikes documentation for creating a API id.
When pushing to the maser branch the CI/CD pipeline should launch a python docker container and run the ```rtrsync.py``` script which will sync the git repo to CrowdStike.

## Classification

Files under the ```powershell``` directory get classified in CrowdStrike as able to run on windows.  
Files under the ```bash``` directory get classified in CrowdStrike as able to run on Linux.  
Files under the ```put``` directory are uploaded to CrowdStrike for use in the RTR command put.  
Please make sure your file is named with the proper extension as this is how the CI/CD python script determines which classification the file gets. Example: ScriptName.ps1  
## WARNING
I have only tested .txt and .ps1 for the put directory. Larger files might still need to be uploaded via a web browser.

This is my first-time using CI/CD and I am in no way a python developer. There may be bugs.

## To do
Rework as a class and cut down on duplicate code.
