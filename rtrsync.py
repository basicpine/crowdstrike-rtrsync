import requests
import os
client_id = os.getenv("client_id")
client_secret = os.getenv("client_secret")
#For Gening a OAUTH Token
def GenToken(client_id,client_secret):
       URL = 'https://api.crowdstrike.com/oauth2/token'
       HEADERS = {
           'Content-Type': 'application/x-www-form-urlencoded',
           'Accept': 'application/json'
       }
       DATA = {
           'client_id':  client_id,
           'client_secret': client_secret
       }
       try:
           result = requests.request("POST", URL, data=DATA, headers=HEADERS)
           result = result.json()
           ACCESS_TOKEN = result['access_token']
           return ACCESS_TOKEN
       except Exception as e:
           error = "web request failed error: " + str(e)
           sys.exit("Check your client secrets.")
           return error
#RTR SCRIPT UPLOADING
def ScriptGet (ACCESS_TOKEN,file):
    HEADERS = {
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/real-time-response/queries/scripts/v1?filter=name:"+"'"+file+"'"
    result = requests.get(url=URL, headers=HEADERS)
    try:
        results = result.json()['resources'][0]
        if results != None:
            return results
        else:
            results = result.json()
            print("FAILED TO GET SCRIPT ID:")
            print(results)
            exit()
    except:
            results = result.json()
            print("FAILED TO MAKE API CALL GET ID")
            print(results)
            exit()
def ScriptReplace(ACCESS_TOKEN,file,ScriptId,dir):
    if file.split('.')[1] == "sh":
        platform = "linux"
    else:
        platform = "windows"
    FILES = {
        'file': (file, open(os.path.join(dir,file), 'rb')),
        'id': (None,ScriptId),
        'description': (None,'replaced with gitlab'),
        'name': (None,file),
        'comments_for_audit_log':(None, 'uploading this file'),
        'permission_type': (None, 'public'),
        'platform': (None, platform)  
    }
    HEADERS = {
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/real-time-response/entities/scripts/v1"
    result = requests.patch(url=URL, files=FILES, headers=HEADERS)
    results = result.json()
    return results
def ScriptUpload (ACCESS_TOKEN,file,dir):
    if file.split('.')[1] == "sh":
        platform = "linux"
    else:
        platform = "windows"

    FILES = {
        'file': (file, open(os.path.join(dir,file), 'rb')),
        'description': (None,'uploaded from gitlab'),
        'name': (None,file),
        'comments_for_audit_log':(None, 'uploaded from git lab'),
        'permission_type': (None, 'public'),
        'platform': (None, platform) 
    }
    HEADERS = {
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/real-time-response/entities/scripts/v1"
    result = requests.post(url=URL, files=FILES, headers=HEADERS)
    results = result.json()
    return results
# FOR PUT FILES
def PutReplace(ACCESS_TOKEN,putid,file):
    HEADERS = {
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/real-time-response/entities/put-files/v1?ids="+putid
    requests.delete(url=URL, headers=HEADERS)
    upload = PutUpload(ACCESS_TOKEN,file)
    return(upload)
def PutID(ACCESS_TOKEN, file):
    HEADERS = {
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/real-time-response/queries/put-files/v1?filter=name:"+"'"+file+"'"
    result = requests.get(url=URL, headers=HEADERS)
    try:
        results = result.json()['resources'][0]
        if results != None:
            return results
        else:
            results = result.json()
            print("FAILED TO GET PUT ID:")
            print(results)
            exit()
    except:
            results = result.json()
            print("FAILED TO MAKE API CALL PUT ID")
            print(results)
            exit()
def PutUpload(ACCESS_TOKEN,file):
    FILES = {
        'file': (file, open(os.path.join("./put",file), 'rb')),
        'description': (None,'uploaded from gitlab'),
        'name': (None,file),
        'comments_for_audit_log':(None, 'uploaded from git lab'),
        'permission_type': (None, 'public'),
    }
    HEADERS = {
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/real-time-response/entities/put-files/v1"
    result = requests.post(url=URL, files=FILES, headers=HEADERS)
    results = result.json()
    return results
Token = GenToken(client_id,client_secret) #Generates a oauth token using client secret snd client id
scripts = ["./powershell","./bash"] #Directorys to look for scripts
for dir in scripts:                 #Loop through the directorys
    listOfFiles = os.listdir(dir)   #Open the directorys
    for file in listOfFiles:        # List all the files in the directory
        getr = ScriptUpload(Token,file, dir) #Try to upload the file to RTR Scripts
        try:
            if getr['errors'][0]['code'] == 409:
                ScriptId = ScriptGet(Token, file)                 #Getting the files ID for a replace
                replace = ScriptReplace(Token,file,ScriptId,dir), #Replacing a file is a diffrent api call
        except:
            print(getr)
            pass
ListOfPutFiles = os.listdir('./put') #Diffrent set of APIs used for put files
for pfile in ListOfPutFiles:         #List the put files
    p = PutUpload(Token,pfile)       #Try to upload the put files
    try:
        if p['errors'][0]['code'] == 409:
            pid = PutID(Token, pfile)              #Getting the files ID for a replace
            preplace = PutReplace(Token,pid,pfile) #Replacing a file is a diffrent api call
    except:
        pass